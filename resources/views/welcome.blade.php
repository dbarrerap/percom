@extends('layouts.app')

@section('title', 'Home')

@section('content')
<div class="text-center">
<h1>{{ config('app.name') }}</h1>
</div>
@endsection