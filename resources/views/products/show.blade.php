@extends('layouts.app')

@section('title', $product->name)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <table class="table">
                <tr>
                    <th>Size</th>
                    <td>{{ $product->size }} {{ $product->measurement }}</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $product->description }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-outline-secondary">Update</a>
            <form action="{{ route('products.destroy', $product->id) }}" method="post" style="display: inline;">
                @csrf
                @method('DELETE')
                <button class="btn btn-outline-danger" type="submit">Delete</button>
            </form>
        </div>
    </div>
</div>
@endsection