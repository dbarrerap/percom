@extends('layouts.app')

@section('title', 'New Product')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <form action="{{ route('products.store') }}" method="post">
        @csrf
        <div class="form-row">
            <div class="form-group col-md"><label for="name">Name</label><input type="text" name="name" id="name" class="form-control" value="{{ $product->name }}"></div>
            <div class="form-group col-md"><label for="size">Size</label><input type="text" name="size" id="size" class="form-control" value="{{ $product->size }}"></div>
            <div class="form-group col-md">
                <label for="measurement">Units</label>
                <select name="measurement" id="measurement" class="form-control">
                @foreach ($measurement as $measure)
                @if ($measure === $product->measurement)
                    <option value="{{ $measure }}" selected>{{ $measure }}</option>
                @else
                    <option value="{{ $measure }}">{{ $measure }}</option>
                @endif
                @endforeach
                </select>
            </div>
        </div>
        <div class="form-group"><label for="description">Description</label><textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $product->description }}</textarea></div>
        <div class="form-group"><button type="submit" class="btn btn-primary mr-3">Save</button><a href="{{ route('products.index') }}" class="btn btn-danger">Cancel</a></div>
        </form>
        </div>
    </div>
</div>
@endsection