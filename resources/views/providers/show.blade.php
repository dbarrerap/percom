@extends('layouts.app')

@section('title', $provider->company)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <table class="table">
            <tr>
                <th>Contact Name:</th>
                <td>{{ $provider->first_name }} {{ $provider->last_name }}</td>
            </tr>
            <tr>
                <th>Job Title:</th>
                <td>{{ $provider->job_title }}</td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{{ $provider->email }}</td>
            </tr>
            <tr>
                <th>Phone:</th>
                <td>{{ $provider->phone }}</td>
            </tr>
        </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <a href="{{ route('providers.edit', $provider->id) }}" class="btn btn-outline-secondary">Update</a>
            <form action="{{ route('providers.destroy', $provider->id) }}" method="post" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-outline-danger" type="submit">Delete</button>
                        </form>
        </div>
    </div>
</div>
@endsection