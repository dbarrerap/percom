@extends('layouts.app')

@section('title', 'Providers')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <a href="{{ route('providers.create') }}" class="btn btn-primary mb-2">New Provider</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($providers as $provider)
                <tr>
                    <td><a href="{{ route('providers.show', $provider->id) }}">{{ $provider->company }}</a></td>
                    <td class="text-right">
                        <form action="{{ route('providers.destroy', $provider->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection