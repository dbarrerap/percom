@extends('layouts.app')

@section('title', 'Update Warehouse')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <form action="{{ route('warehouses.update', $warehouse->id) }}" method="post">
        @csrf
        @method('PATCH')
        <div class="form-group"><label for="name">Company</label><input type="text" name="name" id="name" class="form-control" value="{{ $warehouse->name }}"></div>
        <div class="form-row">
            <div class="form-group col-md"><label for="first_name">First Name</label><input type="text" name="first_name" id="first_name" class="form-control" value="{{ $warehouse->first_name }}"></div>
            <div class="form-group col-md"><label for="last_name">Last Name</label><input type="text" name="last_name" id="last_name" class="form-control" value="{{ $warehouse->last_name }}"></div>
            <div class="form-group col-md"><label for="job_title">Job Title</label><input type="text" name="job_title" id="job_title" class="form-control" value="{{ $warehouse->job_title }}"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md"><label for="email">Email</label><input type="email" name="email" id="email" class="form-control" value="{{ $warehouse->email }}"></div>
            <div class="form-group col-md"><label for="phone">Phone</label><input type="text" name="phone" id="phone" class="form-control" value="{{ $warehouse->phone }}"></div>
        </div>
        <div class="form-group"><button type="submit" class="btn btn-primary mr-3">Update</button><a href="{{ route('warehouses.index') }}" class="btn btn-danger">Cancel</a></div>
        </form>
        </div>
    </div>
</div>
@endsection