@extends('layouts.app')

@section('title', 'Warehouses')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <a href="{{ route('warehouses.create') }}" class="btn btn-primary mb-2">New Warehouse</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($warehouses as $warehouse)
                <tr>
                    <td><a href="{{ route('warehouses.show', $warehouse->id) }}">{{ $warehouse->name }}</a></td>
                    <td class="text-right">
                        <form action="{{ route('warehouses.destroy', $warehouse->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection