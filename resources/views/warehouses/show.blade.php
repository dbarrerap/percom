@extends('layouts.app')

@section('title', $warehouse->name)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <table class="table">
            <tr>
                <th>Contact Name:</th>
                <td>{{ $warehouse->first_name }} {{ $warehouse->last_name }}</td>
            </tr>
            <tr>
                <th>Job Title:</th>
                <td>{{ $warehouse->job_title }}</td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{{ $warehouse->email }}</td>
            </tr>
            <tr>
                <th>Phone:</th>
                <td>{{ $warehouse->phone }}</td>
            </tr>
        </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <a href="{{ route('warehouses.edit', $warehouse->id) }}" class="btn btn-outline-secondary">Update</a>
            <form action="{{ route('warehouses.destroy', $warehouse->id) }}" method="post" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-outline-danger" type="submit">Delete</button>
                        </form>
        </div>
    </div>
</div>
@endsection