@extends('layouts.app')

@section('title', 'Transports')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <a href="{{ route('transports.create') }}" class="btn btn-primary mb-2">New Transport</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($transports as $transport)
                <tr>
                    <td><a href="{{ route('transports.show', $transport->id) }}">{{ $transport->name }}</a></td>
                    <td class="text-right">
                        <form action="{{ route('transports.destroy', $transport->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection