@extends('layouts.app')

@section('title', 'New Transport')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <form action="{{ route('transports.store') }}" method="post">
        @csrf
        <div class="form-group"><label for="name">Company</label><input type="text" name="name" id="name" class="form-control"></div>
        <div class="form-row">
            <div class="form-group col-md"><label for="first_name">First Name</label><input type="text" name="first_name" id="first_name" class="form-control"></div>
            <div class="form-group col-md"><label for="last_name">Last Name</label><input type="text" name="last_name" id="last_name" class="form-control"></div>
            <div class="form-group col-md"><label for="job_title">Job Title</label><input type="text" name="job_title" id="job_title" class="form-control"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md"><label for="email">Email</label><input type="email" name="email" id="email" class="form-control"></div>
            <div class="form-group col-md"><label for="phone">Phone</label><input type="text" name="phone" id="phone" class="form-control"></div>
        </div>
        <div class="form-group"><button type="submit" class="btn btn-primary mr-3">Save</button><a href="{{ route('transports.index') }}" class="btn btn-danger">Cancel</a></div>
        </form>
        </div>
    </div>
</div>
@endsection