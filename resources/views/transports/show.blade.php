@extends('layouts.app')

@section('title', $transport->name)

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <table class="table">
            <tr>
                <th>Contact Name:</th>
                <td>{{ $transport->first_name }} {{ $transport->last_name }}</td>
            </tr>
            <tr>
                <th>Job Title:</th>
                <td>{{ $transport->job_title }}</td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{{ $transport->email }}</td>
            </tr>
            <tr>
                <th>Phone:</th>
                <td>{{ $transport->phone }}</td>
            </tr>
        </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <a href="{{ route('transports.edit', $transport->id) }}" class="btn btn-outline-secondary">Update</a>
            <form action="{{ route('transports.destroy', $transport->id) }}" method="post" style="display: inline;">
                @csrf
                @method('DELETE')
                <button class="btn btn-outline-danger" type="submit">Delete</button>
            </form>
        </div>
    </div>
</div>
@endsection