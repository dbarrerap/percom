<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'company', 'first_name', 'last_name', 'email', 'phone', 'job_title'
    ];
}
