<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [
        'name', 'first_name', 'last_name', 'email', 'phone', 'job_title'
    ];
}
