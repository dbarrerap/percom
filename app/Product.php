<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public static $measurement = ['gr', 'oz', 'unit', 'lb'];

    protected $fillable = [
        'name', 'size', 'measurement', 'description'
    ];
}
